# Partager une vidéo

Pour partager une vidéo, vous devez d'abord aller sur la page vidéo que vous voulez partager. Si vous êtes l'éditeur, vous pouvez lister toutes vos vidéos via l'option _Mes Vidéos_ du menu de gauche. Une fois sur la page vidéo, il vous suffit de cliquer sur le bouton <i data-feather="share-2"></i> **PARTAGER**&nbsp;:

?> les petites icônes en fin de ligne vous permettent de copier l'URL entière en une seule fois.

## URL

L'adresse de la vidéo, par exemple&nbsp;: `https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d`. Cette adresse peut être envoyée à votre contact comme vous le souhaitez, il pourra accéder directement à la vidéo.

![Modale partage url](./assets/fr_share_url.png)

![modale partage url +](./assets/fr_share_url_more.png)

## QR code

Il est possible de partager un vidéo grâce à un QR code&nbsp;:

![modale partage par qr code](./assets/fr_share_qr_code.png)

![modale partage qr code +](./assets/fr_share_qr_code_more.png)

## Intégration

Un code d'intégration qui vous permet d'insérer un lecteur vidéo dans votre site Web.
Lorsqu'un utilisateur partage un lien PeerTube, l'application expose des informations en utilisant le format OEmbed permettant aux sites Web externes d'intégrer automatiquement la vidéo (c'est-à-dire d'injecter le lecteur PeerTube). Certaines plateformes le font avec n'importe quel lien externe (Twitter, Mastodon…) mais d'autres plateformes peuvent nécessiter une configuration supplémentaire.

![modale partage integration](./assets/fr_share_integration.png)

![modale partage integration +](./assets/fr_share_integration_more.png)

## Plus de personnalisation

Pour chaque option, vous pouvez :

  * définir une heure de début en cliquant sur **Début à** et modifier l'horodatage
  * s'il y a des sous-titres, choisir d'en afficher un par défaut en cliquant sur **Sélection automatique des sous-titres**.

Vous avez également la possibilité de personnaliser un peu plus en cliquant sur le bouton **Plus de personnalisation** :

  * **Début à** : choisissez l'heure à laquelle vous souhaitez démarrer la vidéo ;
  * **Stop at** : choisissez l'heure à laquelle vous voulez arrêter la vidéo ;
  * **Autoplay** : cliquez sur ce bouton si vous souhaitez que la vidéo démarre automatiquement ;
  * **Muted** : cliquez sur ce bouton si vous souhaitez que la vidéo soit lue sans son (l'utilisateur peut annuler ce réglage pendant la lecture) ;
  * **Loop** : cliquez si vous voulez que la vidéo soit répétée ;
  * **Afficher le titre de la vidéo** (seulement pour **Embed**) : décochez si vous ne voulez pas afficher le titre de la vidéo ;
  * **Afficher l'avertissement de confidentialité** (uniquement pour **Embed**) : décochez si vous ne voulez pas afficher le message d'avertissement "Regarder cette vidéo peut révéler votre adresse IP à d'autres personnes" ;
  * **Afficher les commandes du lecteur** (uniquement pour **Embed**) : décochez si vous ne voulez pas afficher les boutons lecture/pause, etc.
