# Page not found

Please check that one of your web browser extensions (Privacy Badger, uMatrix, uBlock etc) does not block requests to GitHub.

Some markdown pages are in the [PeerTube GitHub repository](https://github.com/Chocobozzz/PeerTube), so we do cross-origin requests.
